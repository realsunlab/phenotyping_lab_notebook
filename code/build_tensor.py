## construct tensor out of event sequence files

import sys
import os
import pandas as pd
import numpy as np
from collections import OrderedDict
from collections import Counter
sys.path.append("../tensor")
import tensor 
import sptensor
import tensorIO
import SP_NTF

''' ????????????????????????????????
## input and output
### commented out the lines for exmaple files to use for 'file_df_interaction_entries' and 'filename_for_tensor'
#file_df_interaction_entries = '../data_csv_processed/df_PATIENT_ALARM_LEVEL.csv'  
#filename_for_tensor = "../data_tensor/tensor_PATIENT_ALARM_COUNT_20150618-{0}.dat"
'''

'''
## save these parse arguments for use later in the script
file_df_interaction_entries = args.interaction_file
filename_for_tensor = args.result_tensor_filename
aggregation_method = args.aggregation_method
time_axis = args.time_axis
l_tensor_mode_names = args.mode_names
class_label_file = args.class_label_file
bool_bootstrap = args.bool_bootstrap
'''

'''
print "* building tensor"
print "\tfilename for event sequence (interaction) file: " + str(file_df_interaction_entries)
print "\tfilename for tensor (to be saved): " + str( filename_for_tensor)
print "\t aggregation method: " + str(aggregation_method)
print "\t time axis (in the tensor: " + str(time_axis)
print "\t mode names for tensor: " + str(l_tensor_mode_names)
print "\t bootstrap: " + str(bool_bootstrap)
'''

## import CSV data and assign tensor mode names; write them in order; make sure patient is first!!
## if not parsed in, then make the modes numbers corresponding to which column it is
if l_tensor_mode_names == None: #if mode names are supplied
    #df_interaction_entries = pd.read_csv(file_df_interaction_entries, header = None )
    l_tensor_mode_names = range(len(df_interaction_entries.columns)-1)
    df_interaction_entries = df_interaction_entries.rename(columns={len(df_interaction_entries.columns)-1: 'VALUE'})
else: #if mode names supplied
    df_interaction_entries.columns  = l_tensor_mode_names+['VALUE']

df_interaction_entries_backup = df_interaction_entries.copy()

## if BOOTSTRAP: then keep 80% of the patient IDs
print df_interaction_entries.head()

l_pt_ids = np.unique(df_interaction_entries['PATIENT'])

if bool_bootstrap == 1:
    l_pt_ids_keep = l_pt_ids[np.random.choice(len(l_pt_ids), np.floor(len(l_pt_ids)), replace = False)]
    df_interaction_entries = df_interaction_entries[df_interaction_entries['PATIENT'].isin(l_pt_ids_keep)]



## calculate number of modes
num_modes = len(l_tensor_mode_names)


## loop through the names of the modes and create the axis dictionaries
print "loop through the names of the modes and create the axis dictionaries"
axisDict = {}
for k in range(num_modes): #loop through each of the modes
    name = l_tensor_mode_names[k]
    l_unique_items_this_mode = list(np.sort(np.unique(df_interaction_entries[name])))
    od_this_mode = OrderedDict()
    for i in range(len(l_unique_items_this_mode)):
        od_this_mode[l_unique_items_this_mode[i]] = i
    axisDict[k] = od_this_mode # add to the axisDict

## build new dataframe that captures the aggregated data (based on user-specified parameters)
print "build new dataframe that captures the aggregated data (based on user-specified parameters)"
patient_mode_column_indicator_name = l_tensor_mode_names[0]
if aggregation_method == 'MEAN':
    df_interaction_aggro =  df_interaction_entries.groupby(l_tensor_mode_names)['VALUE'].mean().reset_index()
elif aggregation_method == 'BINARY': 
    df_interaction_aggro =  df_interaction_entries.groupby(l_tensor_mode_names).count().reset_index()
    df_interaction_aggro['VALUE'] = [1]*len(df_interaction_aggro)
else: ## default is to do counts
    df_interaction_aggro =  df_interaction_entries.groupby(l_tensor_mode_names).count().reset_index()


## build np array with the interactions
print "* construct tensor: building ll_interactions ..... "
print "# events to parse: " + str(len(df_interaction_aggro))
ll_interactions = np.zeros((len(df_interaction_aggro),len(l_tensor_mode_names) + 1))
for i in range(len(df_interaction_aggro)):
    if np.mod(i, 10000) == 0:
        print i
    l_indexes_for_this_interaction = []
    for k in range(len(l_tensor_mode_names)): #loop through the modes
        mode_name = l_tensor_mode_names[k]
        item_name_on_this_mode = df_interaction_aggro.iloc[i][mode_name]
        index_of_this_item_in_axisDict = axisDict[k][item_name_on_this_mode]
        l_indexes_for_this_interaction.append(index_of_this_item_in_axisDict)

    this_interaction_val = df_interaction_aggro.iloc[i]["VALUE"]
    #check if VALUE is a real number (to filter out NaN's; if not real nubmer, skip to next event)
    if this_interaction_val != this_interaction_val:
        print "VALUE is NaN"
    else:
        #check if each index is a real number (to filter out NaN's)
        l_indexes_real_or_not = []
        for k in range(len(l_indexes_for_this_interaction)): 
            index = l_indexes_for_this_interaction[k]
            if index == index: ## check if it is a real number
                l_indexes_real_or_not.append(1)
            else:
                l_indexes_real_or_not.append(0)
        if np.sum(l_indexes_real_or_not) == len(l_indexes_real_or_not): #if all indexes are real
            l_this_interaction_with_score = list(l_indexes_for_this_interaction)
            l_this_interaction_with_score.append(this_interaction_val)
            ll_interactions[i,:] = l_this_interaction_with_score

np.savetxt( SAVE_FOLDER  + '/event_sequence_processed/' +'TENSOR_INDEX_VALUES.csv', ll_interactions, delimiter=',', fmt='%1.3e')

## BUILD TENSOR FOR PATIENT, MODE1, MODE2, etc etc. ##
nparr_count_tensor = np.array([])
l_data_values = []

## wavelet function setup -- in case we want to use wavelet values for the features over time
if aggregation_method == 'WAVELET': #if using wavelet, then need to apply wavelet transform first
    ## NOTE: still need to implement this section
    ## do wavelet stuff on the values (WILL BE IN COUNTS)
    ## this will yield a DENSE matrix

    #for each patient, do dwt FOR EACH alarm on that time axis
    nparr_count_tensor = np.append( nparr_count_tensor, np.array(indexes)) #needs to be generated
    l_data_values.append(item[-1]) #needs to be generated

else: #if NOT using wavelet, just take the sparse values we have in there
    nparr_count_tensor = ll_interactions[:, 0:ll_interactions.shape[1]-1] #all columsn for indexes
    l_data_values = ll_interactions[:, ll_interactions.shape[1]-1] #last column, for values

print "nparr_count_tensor:    "
print nparr_count_tensor
print "nparr_count_tensor shape: "
print nparr_count_tensor.shape
print "l_data_values: "
print l_data_values
print "* construct tensor: create and save tensor data structure ..... ..... ..... ..... ..... ..... ..... ..... "
print "* num modes: " + str(num_modes)
print "nparr_count_tensor = np.reshape(nparr_count_tensor, [len(nparr_count_tensor) / num_modes, num_modes]).astype(int) #divide by the number of modes;"
nparr_count_tensor = nparr_count_tensor.astype(int) #divide by the number of modes; 

print "nparr_data_values = np.reshape( np.array(l_data_values), [len(l_data_values), 1]).astype(int) #take the values from l_data_values (which are SCORES or COUNTS, etc), reshape as a long vector"
nparr_data_values = np.reshape(np.array(l_data_values), [len(l_data_values), 1]).astype(int) #take the values from l_data_values (which are SCORES or COUNTS, etc), reshape as a long vector

print "sparse_tensor = sptensor.sptensor(nparr_count_tensor, nparr_data_values) #build a tensor with the indexes and data values (which are symptom scores)"
sparse_tensor = sptensor.sptensor(nparr_count_tensor, nparr_data_values) #build a tensor with the indexes and data values (which are symptom scores)

print "od_PATIENTID_idx = axisDict[0]"
od_PATIENTID_idx = axisDict[0]

'''
print "l_patient_classes = [1]*len( od_PATIENTID_idx ) # make each patient have a class of '1'; count the number of patients in the axisDict to make a long vector "
l_patient_classes = [1]*len( od_PATIENTID_idx ) # make each patient have a class of "1"; count the number of patients in the axisDict to make a long vector
'''
print "read in class labels if we have it otherwise set them all to 1"
if class_label_file:
    df_patient_classes = pd.read_csv(class_label_file, header = None)
else:
    df_patient_classes = pd.DataFrame([])
    df_patient_classes[0] = list(np.unique(  df_interaction_entries[df_interaction_entries.columns[0]]  ))
    df_patient_classes[1] = [1] * len(df_patient_classes)

print "d_patient_class = dict(zip(list(df_patient_classes[0]), list(df_patient_classes[1]) ) )"
d_patient_class = dict(zip(list(df_patient_classes[0]), list(df_patient_classes[1]) ) )
print "l_patient_classes = [ d_patient_class[pt] for pt in od_PATIENTID_idx.keys() ]"
l_patient_classes = [ d_patient_class[pt] for pt in od_PATIENTID_idx.keys() ]


print "od_target_classes = OrderedDict( zip(od_PATIENTID_idx.keys(), l_patient_classes))"
od_target_classes = OrderedDict( zip(od_PATIENTID_idx.keys(), l_patient_classes))

print "tensorIO.save_tensor( sparse_tensor, axisDict, od_target_classes, filename_for_tensor)"
tensorIO.save_tensor( sparse_tensor, axisDict, od_target_classes, filename_for_tensor)

