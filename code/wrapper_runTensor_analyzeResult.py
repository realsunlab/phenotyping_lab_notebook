import os
import sys
import json


# set number of iterations to be 1, if not specified by the user
if num_iter == None:
    num_iter = 1
print 'num_iter = '
print num_iter

# rank to use for decomposition
if RANK_RANGE:
    RANK_LOW = RANK_RANGE[0]
    RANK_HIGH = RANK_RANGE[1]
    expt_multiple = bool(expt_multiple)
if expt_multiple:
    if len(RANK_RANGE) != 2:
        sys.exit('invalid range for RANK_RANGE; should have two values only')
    elif RANK_RANGE[1] < RANK_RANGE[0]:
        sys.exit('invalid range for RANK_RANGE')
else:
    RANK = RANK
    if RANK != RANK:
        sys.exit('RANK for decomposition (single rank) not specified')


# parameters to use for NTF
input_alpha = np.float(alpha)
input_gamma = gamma
input_gamma = [np.float(x) for x in input_gamma]

output_prefix = input_tensor_file.split("-{0}.dat")[0].split("data_tensor/")[1] 

'''
????????????? see below
'''
## hard code these dictionaries for now ##NOTE: get rid of these two!
d_reason_occurrences_percent = dict()
d_med_occurrence_percent = dict()


## string for indicating gammas used 
gamma_str = '_alpha_' + str(input_alpha) + '_gamma'
for g in input_gamma:
    gamma_str = gamma_str + '_'
    gamma_str = gamma_str + str(g)



## run expt
results_NTF_files_folder = save_results_location + "NTF_files/" 

if not os.path.exists(results_NTF_files_folder):
    os.mkdir(results_NTF_files_folder)

if expt_multiple==1:
    print "multiple"
    run_expt_multiple(input_tensor_file, results_NTF_files_folder,  output_prefix, RANK_LOW, RANK_HIGH, alpha=input_alpha, gamma=input_gamma, num_iterations=num_iter)
else:
    print "one rank"
    run_expt(input_tensor_file, results_NTF_files_folder,  output_prefix, RANK, alpha=input_alpha, gamma=input_gamma, num_iterations=num_iter) ## change num_iterations for more/less iterations

## analyze results, make xlsx files

file_results_json = results_NTF_files_folder + output_prefix + gamma_str + ".json"
file_results_output_prefix = results_NTF_files_folder +  output_prefix + gamma_str
folder_xlsx_file_save = save_results_location 
file_xlsx_file_save_prefix = folder_xlsx_file_save + output_prefix + gamma_str

print "will save results to: " + folder_xlsx_file_save
if not os.path.exists(folder_xlsx_file_save):
    os.mkdir(folder_xlsx_file_save)


#for i in range(RANK,RANK+1):
if expt_multiple:
    for i in range(RANK_LOW, RANK_HIGH+1):
        for k in range(num_iter):
            analyzeMarble(file_results_json, file_results_output_prefix + '-{0}.dat' + '-iter-' + str(k), file_xlsx_file_save_prefix + '_R_' + str(i) + '-iter-' + str(k) +  '.xlsx', R = i, d_reason_occurrence_percent = d_reason_occurrences_percent, d_med_occurrence_percent = d_med_occurrence_percent, file_raw_event_data = file_raw_event_data, original_tensor = input_tensor_file, class_label_name= class_label_name)
#    for i in range(RANK_LOW, RANK_HIGH+1):
#        analyzeMarble(file_results_json, file_results_output_prefix + '-{0}.dat', file_xlsx_file_save_prefix + '_R_' + str(i) + '.xlsx', R = i, d_reason_occurrence_percent = d_reason_occurrences_percent, d_med_occurrence_percent = d_med_occurrence_percent, file_raw_event_data = file_raw_event_data)


else:
    for i in range(RANK, RANK+1):
        analyzeMarble(file_results_json, file_results_output_prefix + '-{0}.dat', file_xlsx_file_save_prefix + '_R_' + str(i) + '.xlsx', R = i, d_reason_occurrence_percent = d_reason_occurrences_percent, d_med_occurrence_percent = d_med_occurrence_percent, file_raw_event_data = file_raw_event_data, original_tensor = input_tensor_file, class_label_name= class_label_name)


