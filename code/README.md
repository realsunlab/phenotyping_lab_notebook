# code directory

* this directory should include specific code or libraries that are to be used in the phenotyping process.
* this directory should be customized based on your specific goals
* code in this directory can be called in the lab notebook
