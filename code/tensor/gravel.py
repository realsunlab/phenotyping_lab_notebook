'''
Compute the simultaneous nonnegative multi-tensor factorization
Objective function is the sum of (generalized) KL divergence
This is extension of Granite to multi-tensor domain
'''
import numpy as np
from abc import ABCMeta, abstractmethod

import tensorTools
from ktensor import ktensor
from sptensor import sptensor
import marbleBCD as mbd


def _chk_mat_shape(matrix, row_num, col_num):
    """
    Check the shape of the matrix has specific # cols and rows
    """
    if matrix.shape[0] != row_num:
        return False
    if matrix.shape[1] != col_num:
        return False
    return True


def _increasing(L):
    """
    Check if a list of numbers (or arrays) is strictly increasing
    """
    return all(x < y for x, y in zip(L, L[1:]))


def _chk_mem_mat(matrix):
    """
    Check the membership matrix for increasing indices
    """
    for row in range(matrix.shape[0]):
        modeIdx = np.extract(matrix[row, :] > 0, matrix[row, :])
        if not _increasing(modeIdx):
            return False
    return True


def _compute_kl(X, Z, lhat, Ahat):
    """
    Compute KL divergence
    """
    if isinstance(X, sptensor):
        # do a quick sanity check
        if any(Z == 0):
            raise ValueError("Unexpected zero in Z: exit _compute_kl")
        ll = np.sum(Ahat[0] * lhat[np.newaxis, :]) - \
            np.sum(np.multiply(X.vals.flatten(), np.log(Z)))
    else:
        ll = np.sum(Z._data.flatten() -
                    X._data.flatten() * np.log(Z._data.flatten()))
    return ll


def _compute_ls(X, Z, lhat, Ahat):
    """
    Compute the frobenius norm
    """
    normX = X.norm() ** 2
    approx = ktensor(lhat, Ahat)
    normZ = approx.norm() ** 2
    return normX + normZ - 2 * approx.innerprod(X)


def _compute_logistic(X, Z, lhat, Ahat):
    """
    Logistic loss
    """
    if isinstance(X, sptensor):
        firstTerm = np.sum(np.log(1 + np.exp(Ahat[0] * lhat[np.newaxis, :])))
        secondTerm = np.sum(np.multiply(X.vals.flatten(), Z))
    else:
        firstTerm = np.sum(np.log(1 + np.exp(Z._data.flatten())))
        secondTerm = np.sum(X._data.flatten() * np.log(Z._data.flatten()))
    return firstTerm - secondTerm


class Gravel(object):
    X = None             # Observed tensor
    loss = None          # The loss associated with each type
    R = 0                # CP decomposition rank
    M = None             # membership matrix
    G = 0                # number of unique modes
    K = 0                # number of observed tensors
    dim_modes = []       # the dimension of the modes
    A = None             # the factor matrices
    u = None             # the bias factors
    lmbda = None         # the factor weights / tensor
    sigma = None         # the bias weights / tensor
    Z = None             # the approximation for each k
    obj_k = None         # the last known log likelihood for each tensor

    __metaclass__ = ABCMeta

    def initialize(self, fMats=None, biasMats=None):
        if fMats is not None:
            if not all(_chk_mat_shape(fMats[m], modeSize, self.R)
                       for m, modeSize in enumerate(self.dim_modes)):
                raise ValueError("Initalization factors shape is wrong")
            self.A = fMats
        else:
            self.A = tensorTools.random_factors(self.dim_modes, self.R)
        if biasMats is not None:
            if not all(_chk_mat_shape(biasMats[m], modeSize, self.R)
                       for m, modeSize in enumerate(self.dim_modes)):
                raise ValueError("Initalization factors shape is wrong")
            self.u = biasMats
        else:
            self.u = tensorTools.random_factors(self.dim_modes, 1)
        self.lmbda = [np.random.uniform(low=50, high=100, size=self.R)
                      for k in range(self.K)]
        self.sigma = [np.random.uniform(low=50, high=100, size=1)
                      for k in range(self.K)]
        return

    def _get_negative_n(self, k, g):
        """
        Calculate all the non-g modes in this tensor
        """
        othModes = np.flatnonzero(self.M[k, :])
        modeIdx = np.where(othModes == g)
        return np.delete(othModes, modeIdx)

    def get_mode_idx(self, k):
        """
        Get the unique modes from tensor k
        """
        return np.flatnonzero(self.M[k, :])

    def get_tensor_idx(self, g):
        """
        Get the indices of the tensors which mode g contributes
        """
        return np.flatnonzero(self.M[:, g])

    def _update_Z(self, k, lHat, AHat):
        return mbd.compute_Z(self.X[k], lHat, AHat)

    def load(self, filename):
        """
        Load the decomposition details (all but the tensor)
        """
        infile = open(filename, "rb")
        self.R = np.load(infile)
        self.M = np.load(infile)
        self.dim_modes = np.load(infile)
        self.A = np.load(infile)
        self.u = np.load(infile)
        self.lmbda = np.load(infile)
        self.sig = np.load(infile)
        infile.close()

    def save(self, filename):
        """
        Save the decomposition details
        """
        outfile = open(filename, "wb")
        np.save(outfile, self.R)                # save the rank
        np.save(outfile, self.M)                # save the membership info
        np.save(outfile, self.dim_modes)        # save the mode dimensinos
        np.save(outfile, self.A)                # save the factor matrix
        np.save(outfile, self.u)                # save the bias matrix
        np.save(outfile, self.lmbda)            # save the weights
        np.save(outfile, self.sigma)            # save the lmbda
        outfile.close()

    def compute_total_obj(self):
        """
        Computes the total objective function by summing
        the individual objective_k values
        """
        return np.sum(self.obj_k)

    def compute_obj_k(self, k, Z, lhat, Ahat):
        """
        Compute the objective associated with k
        """
        ll = 0
        if self.loss[k] == "gaussian":
            ll = _compute_ls(self.X[k], Z, lhat, Ahat)
        elif self.loss[k] == "poisson":
            ll = _compute_kl(self.X[k], Z, lhat, Ahat)
        elif self.loss[k] == "bernoulli":
            ll = _compute_logistic(self.X[k], Z, lhat, Ahat)
        else:
            raise NotImplementedError("Loss function not implemented")
        return ll

    @abstractmethod
    def compute_decomp(self, **kwargs):
        pass
