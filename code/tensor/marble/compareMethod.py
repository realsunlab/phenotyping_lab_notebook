"""
Experiment to compare Marble with CP-APR

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) the size of each dimension
fl:     (optional) the non-zero percentage along each dimension
g:      (optional) the minimum non-zero entry value
"""
import json
import argparse
import time
import numpy as np
import sys
sys.path.append("..")

import tensorTools
from marbleAPR import MarbleAPR
import CP_APR
import simultTools

INNER_ITER = 5
MAX_ITER = 500
_NUM_SAMPLES = 10
_DEF_ALPHA = 0


def comp_sim_metrics(TM, M):
    fms = TM.greedy_fms(M)
    fos = TM.greedy_fos(M)
    nnz = tensorTools.count_ktensor_nnz(M)
    return fms, fos, nnz


def run_type(type, X, R, seed, maxiters, TM, **kwargs):
    gamma = kwargs.pop('gamma', list(np.repeat(0, X.ndims())))
    alpha = kwargs.pop('alpha', _DEF_ALPHA)
    np.random.seed(seed)
    startTime = time.time()
    if type == "Marble":
        spntf = MarbleAPR(X, R=R, alpha=alpha)
        spntf.compute_decomp(max_inner=INNER_ITER, gamma=gamma, gradual=True,
                             max_iter=maxiters)
        fms, fos, nnz = comp_sim_metrics(TM, spntf.M[0])
    elif type == "CP_APR":
        YCP, _, _ = CP_APR.cp_apr(X, R=R, maxinner=INNER_ITER,
                                  maxiters=maxiters)
        fms, fos, nnz = comp_sim_metrics(TM, YCP)
    elif type == "Limestone":
        YCP, _, _ = CP_APR.cp_apr(X, R=R, maxinner=INNER_ITER,
                                  maxiters=maxiters)
        for n in range(YCP.ndims()):
            YCP.U[n] = tensorTools.hardThresholdMatrix(YCP.U[n], gamma[n])
        fms, fos, nnz = comp_sim_metrics(TM, YCP)
    elapsedTime = time.time() - startTime
    return {"time": elapsedTime, "fms": fms, "fos": fos, "nnz": nnz}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("alpha", type=int, help="alpha")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-g", '--gamma', nargs='+', type=float, help="gamma")
    args = parser.parse_args()

    exptID = args.expt
    R = args.r
    alpha = args.alpha
    MSize = args.MS
    gamma = args.gamma
    AFill = args.fill

    print "Generating simulation data with known decomposition"
    ## generate the solution
    TM, TMHat = simultTools.gen_solution(MSize, R, AFill, alpha)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    ## generate an observation from the known solution
    X = simultTools.gen_random_problem(TMFull)

    RESULT_DICT = {'expt': 'method', 'id': exptID, 'size': MSize,
                   'sparsity': AFill, "rank": R, "alpha": alpha,
                   "gamma": gamma}

    outfile = open('results/simulation-{0}.json'.format(exptID), 'w')

    for sample in range(_NUM_SAMPLES):
        output = RESULT_DICT
        output['sample'] = sample
        seed = sample * 1000
        marbleResult = run_type("Marble", X, R, seed, MAX_ITER, TM,
                                alpha=alpha, gamma=gamma)
        output['marble'] = marbleResult
        cpaprResult = run_type("CP_APR", X, R, seed, MAX_ITER, TM)
        output['CP_APR'] = cpaprResult
        cpaprResult = run_type("Limestone", X, R, seed, MAX_ITER, TM,
                               gamma=gamma)
        output['Limestone'] = cpaprResult
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
