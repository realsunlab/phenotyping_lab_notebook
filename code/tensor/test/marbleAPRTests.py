import sys
sys.path.append("..")

import marbleAPR        # noqa
import marble           # noqa
import marbleBCD        # noqa
import simultTools      # noqa
from ktensor import ktensor   # noqa


R = 5
alpha = 10
MSize = [20, 10, 10]
AFill = [5, 4, 3]

# generate the solution
TM, TMHat = simultTools.gen_nonoverlap_soln(MSize, R, AFill,
                                            alpha, [0.5, 0.4, 0.4])
TMFull = TM.to_dtensor() + TMHat.to_dtensor()
# generate an observation from the known solution
X = simultTools.gen_random_problem(TMFull)

spntf = marbleAPR.MarbleAPR(X, R, alpha)
spntf.compute_decomp()
print "Similarity:" + str(TM.fms(spntf.cp_decomp[0]))
print "LL:" + str(marble.calculate_ll(X, spntf.cp_decomp))

# spntf = marbleAPR.MarbleAPR(X.to_dtensor(), R, alpha)
# spntf.compute_decomp()

spntf = marbleBCD.MarbleBCD(X, R)
spntf.compute_decomp(max_iter=150)
A = ktensor(spntf.cp_decomp.lmbda[:-1],
            [spntf.cp_decomp.U[n][:, :-1] for n in range(3)])
print "Similarity:" + str(TM.fms(A))
print "LL:" + str(marble.calculate_ll(X, spntf.cp_decomp))


# spntf = marbleAPR.MarbleAPR(X.to_dtensor(), R, alpha)
# spntf.compute_decomp()

# tmarb = tmarble.TMarbleAPR(X, R, alpha, AFill)
# tmarb.compute_decomp()
# print "Similarity:" + str(TM.fms(tmarb.cp_decomp[0]))
# print "LL:" + str(marble.calculate_ll(X, tmarb.cp_decomp))


# tmarb = tmarble.TMarbleAPR(X, R, alpha, AFill)
# tmarb.compute_decomp()
# print "Similarity:" + str(TM.fms(tmarb.cp_decomp[0]))
# print "LL:" + str(marble.calculate_ll(X, tmarb.cp_decomp))

# marbSim = marbleSim.MarbleSim(X, R, alpha)
# marbSim.compute_decomp(step_lambda=500)
# print "Similarity:" + str(TM.fms(marbSim.cp_decomp[0]))
# print "LL:" + str(marble.calculate_ll(X, marbSim.cp_decomp))
