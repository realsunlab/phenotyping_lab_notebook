import sys
sys.path.append("..")

from graniteBCD import GraniteBCD      # noqa
from graniteBCD import GraniteAM      # noqa
import simultTools                     # noqa


MSize = [20, 15, 10]
AFill = [10, 8, 5]
R = 3
theta = [0.6, 0.3, 0.2]
beta1 = 0
beta2 = 5
max_iter = 100
alpha = 100
TM, TMHat = simultTools.gen_nonoverlap_soln(MSize, R, AFill, alpha,
                                            theta, 500, 1000)
TMFull = TM.to_dtensor() + TMHat.to_dtensor()
X = simultTools.gen_random_problem(TMFull)
print "-------- Granite BCD --------"
spntf = GraniteBCD(X, R=R, alpha=0)
spntf.compute_decomp(cos_reg=beta2, max_iter=max_iter, theta=theta)
print "Similarity:" + str(TM.fms(spntf.get_signal_factors()))
projMat = spntf.project_data(X, 0)
print "-------- Granite AM --------"
spntf = GraniteAM(X, R=R, alpha=0)
spntf.compute_decomp(cos_reg=beta2, max_iter=max_iter, theta=theta)
print "Similarity:" + str(TM.fms(spntf.get_signal_factors()))
