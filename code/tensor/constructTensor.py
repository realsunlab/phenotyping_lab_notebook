import csv
from collections import defaultdict, OrderedDict
import numpy as np
from sptensor import sptensor


class ConstructTensor(object):
    """
    Class to construct a tensor from a file
    """
    axisIdx = None
    valIdx = None
    axisDict = None

    def __init__(self, axisIdx, valIdx, axisDict=None):
        self.axisIdx = axisIdx
        self.valIdx = valIdx
        self.axisDict = axisDict
        if axisDict is None:
            self.axisDict = defaultdict(lambda: OrderedDict(sorted({}.items(),
                                                            key=lambda t: t[1])))   # noqa

    def parseRow(self, line, N):
        rowIdx = []
        for n in range(N):
            axisKey = line[self.axisIdx[n]]
            # add to the dictionary if it doesn't exist
            if axisKey not in self.axisDict[n]:
                self.axisDict[n][axisKey] = len(self.axisDict[n])
            rowIdx.append(self.axisDict[n].get(axisKey))
        val = float(line[self.valIdx])
        return True, rowIdx, val

    def parseFile(self, filename, sep=","):
        N = len(self.axisIdx)
        tensorIdx = np.zeros((1, N), dtype=int)
        tensorVal = np.array([[0]], dtype=float)
        f = open(filename, "rb")
        for row in csv.reader(f, delimiter=sep):
            writeRow, rowIdx, val = self.parseRow(row, N)
            if writeRow:
                tensorIdx = np.vstack((tensorIdx,
                                       np.array([rowIdx], dtype=int)))
                tensorVal = np.vstack((tensorVal, [[val]]))
        f.close()
        tShape = [len(self.axisDict[n]) for n in range(N)]
        tenX = sptensor(tensorIdx, tensorVal, np.array(tShape))
        return tenX, self.axisDict


class ConstructFixedTensor(ConstructTensor):

    def parseRow(self, line, N):
        rowIdx = []
        for n in range(N):
            axisKey = line[self.axisIdx[n]]
            # add to the dictionary if it doesn't exist
            if axisKey not in self.axisDict[n]:
                return False, None, None
            rowIdx.append(self.axisDict[n].get(axisKey))
        val = float(line[self.valIdx])
        return True, rowIdx, val
