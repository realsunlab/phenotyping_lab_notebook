import numpy as np
from granite import Granite
import granite
import simplex_projection


def truncate_matrix(A, k):
    sortedIdx = np.argsort(A, axis=0)[::-1]
    A[sortedIdx > (k-1)] = 0
    A = granite.fix_A(A)
    A = np.apply_along_axis(simplex_projection.euclidean_proj_simplex,
                            0, A)
    return A


class TGranite(Granite):
    support = None      # the support for each mode

    def __init__(self, X, R, alpha, support):
        self.support = support
        super(TGranite, self).__init__(X, R, alpha)

    def _project_signal(self, A, N):
        return [truncate_matrix(A[n], self.support[n]) for n in range(N)]
