'''
Nonnegative tensor factorization for count data.
Algorithm is described in the Granite.
'''
import numpy as np
from scipy import spatial

from marbleAM import MarbleAM
from marbleBCD import MarbleBCD
import tensorTools as tt

_DEF_COS_REG = 0
_DEF_THETA = 0.2
np.seterr(all='raise', under="ignore")


def cosine_sim(u, v):
    u_norm = np.linalg.norm(u, ord=2)
    v_norm = np.linalg.norm(v, ord=2)
    numerator = np.dot(u, v)
    if not (np.isfinite(u_norm) and np.isfinite(v_norm)):
        print u, v
        raise ValueError("Not finite u, v")
    try:
        cos_val = numerator / (u_norm * v_norm)
    except:
        print u, v
        raise ValueError("Not finite")
    return cos_val


def calc_cosine(A, theta=1):
    """
    Calculate the second function (cosine distance) allowing for
    angular similarty
    """
    cosScore = spatial.distance.cdist(A.T, A.T, cosine_sim)
    np.fill_diagonal(cosScore, 0)
    C = cosScore - theta
    return np.triu(np.maximum(0, C), k=1)


def du_cosine(A, r):
    V = A[:, tt.range_omit_k(A.shape[1], r)]
    u = A[:, r]
    uv = np.dot(u, V)                       # calculates <u, v>
    u_norm = np.linalg.norm(u, ord=2)       # calculates the 2 norm
    V_norm = np.linalg.norm(V, ord=2, axis=0)
    U = np.repeat(u[:, np.newaxis], A.shape[1] - 1, 1)
    top = V * u_norm - np.multiply(uv, U)
    denom = V_norm * u_norm ** 2
    try:
        return top / denom[np.newaxis, :]
    except FloatingPointError:
        print "Error in calculating gradient of cosine", str(A), str(r)
        return 0


def compute_cos_object(A, theta, cos_reg):
    """
    Compute the objective function part with the
    cosine regularization parameter
    """
    ll = 0
    for n in range(len(A)):
        if np.isnan(A[n]).any():
            print A[n]
            raise ValueError("Objective with invalid A")
        cos = calc_cosine(A[n][:, :-1], theta[n])
        ll += cos_reg * np.sum(cos)
    return ll


def compute_cos_grad_A(df1, A, theta, cos_reg):
    if np.isnan(A).any():
        print A
        raise ValueError("passing Nan to calc_cosine")
    R = A.shape[1]
    cos = calc_cosine(A, theta)
    for r in range(R):
        gradA = du_cosine(A, r)
        multA = cos[r, tt.range_omit_k(R, r)]
        gradA = np.sum(multA * gradA, axis=1)
        df1[:, r] = df1[:, r] + cos_reg * gradA
    return df1


class Granite(MarbleBCD):
    """
    Abstract class with defined things
    """
    cos_reg = 0          # the penalty for overangular constraints
    theta = None

    def _grad_A_n(self, n):
        df1 = super(GraniteBCD, self)._grad_A_n(n)
        return compute_cos_grad_A(df1, self.cp_decomp.U[n][:, :-1],
                                  self.theta[n], self.cos_reg)

    def compute_obj(self, Z, l, A):
        ll = super(GraniteBCD, self).compute_obj(Z, l, A)
        ll += compute_cos_object(A, self.theta, self.cos_reg)
        return ll


class GraniteBCD(Granite):
    def compute_decomp(self, **kwargs):
        self.cos_reg = kwargs.pop('cos_reg', _DEF_COS_REG)
        self.theta = kwargs.pop('theta', np.repeat(_DEF_THETA, self.dim_num))
        return super(GraniteBCD, self).compute_decomp(**kwargs)

    def project_data(self, XHat, n, **kwargs):
        origCos = self.cos_reg
        origTheta = self.theta
        self.cos_reg = 0
        self.theta = np.repeat(1, self.dim_num)
        projMat = super(GraniteBCD, self).project_data(XHat, n, **kwargs)
        # set everything back
        self.cos_reg = origCos
        self.theta = origTheta
        return projMat


class GraniteAM(MarbleAM, Granite):
    def compute_decomp(self, **kwargs):
        self.cos_reg = kwargs.pop('cos_reg', _DEF_COS_REG)
        self.theta = kwargs.pop('theta', np.repeat(_DEF_THETA, self.dim_num))
        return super(GraniteAM, self).compute_decomp(**kwargs)

    def project_data(self, XHat, n, **kwargs):
        origCos = self.cos_reg
        origTheta = self.theta
        self.cos_reg = 0
        self.theta = np.repeat(1, self.dim_num)
        projMat = super(GraniteAM, self).project_data(XHat, n, **kwargs)
        # set everything back
        self.cos_reg = origCos
        self.theta = origTheta
        return projMat
