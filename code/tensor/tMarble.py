import numpy as np
import marbleBCD as mbd
import marbleAM as mam
import simplex_projection as simplex
import tensorTools as tt

_DEF_NNZ = 10


def _truncate_k(v, k, s):
    # given a vector, sort and truncate
    sort_idx = np.argsort(v)[::-1]
    newv = np.zeros(v.shape)
    newv[sort_idx[:k]] = simplex.euclidean_proj_simplex(v[sort_idx[:k]], s)
    return newv


def project_mode(A, s, k):
    A = mbd.fix_A(A)                    # fix floating point errors
    try:
        A[:, :-1] = np.apply_along_axis(_truncate_k,
                                        0, A[:, :-1], k=k, s=s)
    except IndexError:
        print A
        raise ValueError("Exiting because simplex is incorrect")
    A[:, -1] = np.maximum(mbd.AUG_MIN, A[:, -1])
    A = tt.col_normalize(A)
    mbd.sanity_check_A(A)
    return A


class TMarble(object):
    nnz = None        # nnz is the sparsity parameter


class TMarbleBCD(TMarble, mbd.MarbleBCD):
    def _project_factors(self, A, N):
        return [project_mode(A[n], s=self.s[n], k=self.nnz[n])
                for n in range(N)]

    def compute_decomp(self, **kwargs):
        self.nnz = kwargs.pop('k', np.repeat(_DEF_NNZ, self.dim_num))
        return super(TMarbleBCD, self).compute_decomp(**kwargs)


class TMarbleAM(TMarble, mam.MarbleAM):
    def _init_factors(self):
        for n in self.othModes[self.absorbMode]:
            self.cp_decomp.U[n] = project_mode(self.cp_decomp.U[n],
                                               s=self.s[n],
                                               k=self.nnz[n])

    def _proj_factors(self, A, n):
        return project_mode(A, s=self.s[n], k=self.nnz[n])

    def compute_decomp(self, **kwargs):
        self.nnz = kwargs.pop('k', np.repeat(_DEF_NNZ, self.dim_num))
        return super(TMarbleAM, self).compute_decomp(**kwargs)
