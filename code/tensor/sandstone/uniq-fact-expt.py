"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import functools
import json
import multiprocessing
import numpy as np
import itertools
import sys
sys.path.append("..")

from ktensor import ktensor               # noqa
from sandstoneAM import SandstoneAM       # noqa
import tensorIO                           # noqa
import tensorTools as tt                  # noqa


X = None                    # declaration to prevent errors
memMat = None               # declaration to prevent errors
modeDim = None              # declaration to prevent errors
bestLL = None               # declaration to prevent errors
llLock = None               # declaration to prevent errors
JSON_UNIQ_OUT = "results/{0}-uniq-{1}.json"   # output file format
JSON_FACT_OUT = "results/{0}-fact-{1}.json"   # output file format
DAT_FACT_OUT = "results/{0}-fact-{1}.dat"     # output file format


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, memMat, modeDim, bestLL, llLock
    X, memMat, modeDim, bestLL, llLock = args


def run_sample(R, s, regL, cosReg, theta, outerIter, innerIter,
               exptID, sample):
    np.random.seed()
    ss = SandstoneAM(X, R, memMat, modeDim)
    _, ll = ss.compute_decomp(max_iter=outerIter, inner_iter=innerIter,
                              reg_l=regL, cos_reg=cosReg, theta=theta)
    print "Sample", sample, ":", ll
    with llLock:
        if ll < bestLL.value:
            ss.save(DAT_FACT_OUT.format("SandstoneAM", exptID))
            bestLL.value = ll
    # Analyze the overlap
    colNNZ = [tt.count_nnz(ss.A[mode], 0) for mode in range(memMat.shape[1])]
    rowNNZ = [tt.count_nnz(ss.A[mode], 1) for mode in range(memMat.shape[1])]
    lmbdaNNZ = [tt.count_nnz(ss.lmbda[k], 0) for k in range(memMat.shape[0])]
    l, A = ss.get_signal_factors()
    return sample, ll, l, A, colNNZ, rowNNZ, lmbdaNNZ


def count_zeros(A):
    zeroIdx = np.where(np.asarray(A.lmbda) == 0)[0]
    return len(zeroIdx)


def modify_ktensor(A, removeK):
    # argsort the lmbdas
    Almbda = np.asarray(A.lmbda)
    idx = np.argsort(Almbda)[::-1]
    # remove all but the end
    lmbda = Almbda[idx[:-removeK]]
    U = [A.U[n][:, idx[:-removeK]] for n in range(len(A.U))]
    return ktensor(lmbda, U)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta",
                        default=None)
    parser.add_argument("-b1", '--beta1', type=float,
                        help="weight regularizer", default=0)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=0)
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    R = args.r
    theta = args.theta
    beta1 = args.beta1
    beta2 = args.beta2
    max_iter = args.iters
    sparsity = args.sparsity
    exptID = args.expt
    inputFile = args.infile
    ii = args.ii
    dt = "SandstoneAM"

    X, memMat, modeDim, axisDict = tensorIO.load_multi_tensor(inputFile)
    uniqOut = open(JSON_UNIQ_OUT.format(dt, exptID), 'w')
    factOut = open(JSON_FACT_OUT.format(dt, exptID), 'w')
    BASE_RESULT = {'decomp': dt, 'id': exptID, "R": R,
                   "input": inputFile, "s": sparsity, 'maxiter': max_iter,
                   "innerIter": ii, 'theta': theta, 'cosReg': beta2,
                   "weightReg": beta1}
    UNIQ_RESULT = BASE_RESULT.copy()
    UNIQ_RESULT.update({'expt': 'uniqueness'})
    FACT_RESULT = BASE_RESULT.copy()
    FACT_RESULT.update({'expt': 'factor'})
    bestLL = multiprocessing.Value('f', sys.float_info.max)
    llLock = multiprocessing.Lock()
    func = functools.partial(run_sample, R, sparsity, beta1, beta2, theta,
                             max_iter, ii, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, memMat, modeDim, bestLL, llLock))
    result = pool.map_async(func, range(args.samples)).get(9999999)
    # write the uniqueness results
    for i, j in itertools.combinations(range(args.samples), 2):
        lA = result[i][2]
        AA = result[i][3]
        lB = result[j][2]
        BB = result[j][3]
        fms = tt.compare_multisite_factors(AA, lA, BB, lB,
                                           memMat.shape[0])
        output = UNIQ_RESULT.copy()
        output.update({"sample": [i, j], "fms": fms})
        uniqOut.write(json.dumps(output) + "\n")
    uniqOut.close()
    for smpRes in result:
        output = FACT_RESULT.copy()
        output.update({"sample": smpRes[0], "LL": smpRes[1],
                       "colNNZ": smpRes[4], "rowNNZ": smpRes[5],
                       "weight": smpRes[6]})
        factOut.write(json.dumps(output) + "\n")
    factOut.close()

if __name__ == "__main__":
    main()
