from collections import OrderedDict
import math
import numpy as np
from sklearn.cross_validation import ShuffleSplit
from sklearn.linear_model import LassoCV
from sklearn import metrics
import sys
sys.path.append("..")

import tensorIO                 # noqa


def _train_glm(trainX, trainY, testX, testY):
    model = LassoCV(cv=20).fit(trainX, trainY)
    model.fit(trainX, trainY)
    trainMSE = metrics.mean_squared_error(trainY, model.predict(trainX))
    testMSE = metrics.mean_squared_error(testY, model.predict(testX))
    trainR2 = metrics.r2_score(trainY, model.predict(trainX))
    testR2 = metrics.r2_score(testY, model.predict(testX))
    return {"train-rmse": math.sqrt(trainMSE), "test-rmse": math.sqrt(testMSE),
            "train-r2": trainR2, "test-r2": testR2}


def get_results(trainX, trainY, testX, testY):
    # get the normal results
    res = {}
    res['normal'] = _train_glm(trainX, trainY, testX, testY)
    res['log'] = _train_glm(trainX, np.log(trainY+1), testX, np.log(testY+1))
    return res


def _get_value_from_dictval(classD, k, kt):
    val = classD[k]
    if kt not in val:
        return float(0)
    else:
        return val[kt]


def load_split(inputFile, testSize, samples, seed=100):
    np.random.seed(seed)
    X, axisDict, classDict = tensorIO.load_tensor(inputFile)
    patOrder = OrderedDict(sorted(axisDict[0].items(), key=lambda t: t[1]))
    inpatientY = [_get_value_from_dictval(classDict, k, "inpatient")
                  for k in patOrder.keys()]
    outpatientY = [_get_value_from_dictval(classDict, k, "outpatient")
                   for k in patOrder.keys()]
    ttss = ShuffleSplit(X.shape[0], n_iter=samples, test_size=testSize)
    return X, np.array(inpatientY), np.array(outpatientY), ttss
