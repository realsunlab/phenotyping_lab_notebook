from pymongo import MongoClient
import pandas
import numpy as np
import argparse


def get_frame(exptType, expt, sample, rank, target, predType, valuesDict):
    """
    Given an array, add it with the experiment type, id, sample
    and parameter
    """
    N = len(valuesDict)
    vals = []
    vals.extend(zip(np.repeat(exptType, N),
                    np.repeat(expt, N),
                    np.repeat(sample, N),
                    np.repeat(rank, N),
                    np.repeat(target, N),
                    np.repeat(predType, N),
                    valuesDict.keys(),
                    valuesDict.values()))
    return vals


def get_apr_expt(mDB, expt):
    vals = []
    for exptRun in mDB.find({"id": expt}):
        if exptRun is None:
            continue    # if you can't find it do nothing
        sample = exptRun['sample']
        exptType = exptRun['expt']
        rank = 0
        if "rank" in exptRun:
            rank = exptRun['rank']
        elif "R" in exptRun:
            rank = exptRun["R"]
        predType = exptRun["type"]
        vals.extend(get_frame(exptType, expt, sample, rank,
                              "log", predType, exptRun['log']))
        vals.extend(get_frame(exptType, expt, sample, rank,
                              "normal", predType, exptRun['normal']))
    return vals


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('maxExpt', type=int, help="maximum index")
    parser.add_argument("output", help="output file")
    args = parser.parse_args()

    mongoC = MongoClient()
    db = mongoC.granite
    mongoDB = db["pred"]                  # load the database collection
    exptRange = range(1, args.maxExpt)    # iterate through all the experiments
    totalDF = []
    for expt in exptRange:
        eNnz = get_apr_expt(mongoDB, expt)
        totalDF.extend(eNnz)
    pdDF = pandas.DataFrame(totalDF, columns=["exptType", "expt", "sample",
                                              "rank", "target", "type", "metric",
                                              "value"])
    with open(args.output, 'w') as outfile:
        pdDF.to_csv(outfile, index=False)


if __name__ == "__main__":
    main()
