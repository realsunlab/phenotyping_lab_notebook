# drop the collection first
mongo gravel --eval "db.multisite.drop()"
for f in results/multisite-sim*.json
do
  echo "Importing file $f..."
  mongoimport --db gravel --collection multisite  --type json --file $f --jsonArray
  echo "Done $f"
done