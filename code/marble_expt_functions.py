from collections import defaultdict
import json
import numpy as np
import sys
sys.path.append("../tensor/")

import tensorIO
#import SP_NTF
import time
import marbleAPR

#def marbleInstance(X, r, alpha, gamma):
#    marble = SP_NTF.SP_NTF(X, r, alpha)
#    info = marble.computeDecomp(gamma=gamma)
#    lastIter = info.keys()[-1]
#    return marble, info[lastIter]['LL']

def marbleInstance(X, r, alpha, gamma):
    marble = marbleAPR.MarbleAPR(X, R=r, alpha=alpha)
    iterInfo, lastLL = marble.compute_decomp(gamma=gamma, gradual=True)
    lastIter = iterInfo.keys()[-1]
    return marble, iterInfo[lastIter]['LL']

def run_Marble(X, r, alpha, gamma, num_iterations=10):
    print "iter: 0"
    tenDecomp, logLL = marbleInstance(X, r, alpha, gamma)
#    logLL = float('inf')
    logLL_allIters = []
    for i in range(1, num_iterations): # run it for set number of iterations
        print "iter: " + str(i)
        xDecomp, xLL = marbleInstance(X, r, alpha, gamma)
        logLL_allIters.append(xLL)
        if xLL > logLL:
            logLL = xLL
            tenDecomp = xDecomp
    return tenDecomp, logLL, logLL_allIters    ## return the best decomposition and the LL stats; mod 20160602 RC: now, add list of all LL's -- did this for Sutter expt


def run_Marble_returnAllIters(X, r, alpha, gamma, num_iterations=10):
    print "doing all iterations of marble for this run....."
    print "number iters total for this run: " + str(num_iterations)
    print ".....\n"

    print "iter: 0"
    best_tenDecomp, best_logLL = marbleInstance(X, r, alpha, gamma)
#    best_logLL = float('inf')
    logLL_allIters = []
    marble_xDecomp_allIters = []

    for i in range(1, num_iterations+1): # run it for set number of iterations
        print "iter: " + str(i)
        xDecomp, xLL = marbleInstance(X, r, alpha, gamma)
        logLL_allIters.append(xLL)
        marble_xDecomp_allIters.append(xDecomp)
        if xLL > best_logLL:
            best_logLL = xLL
            best_tenDecomp = xDecomp
    return best_tenDecomp, best_logLL, marble_xDecomp_allIters, logLL_allIters    ## return the best decomposition and the LL stats; mod 20160602 RC: now, add list of all LL's -- did this for Sutter expt


def run_Marble_bootstrap(X, r, alpha, gamma, num_iterations = 2):
    print "running bootstrap samples of Marble...."
    
    # store list of runs (iterations)
    #    - cluster membership ALONG EACH MODE --> this should be stored in the run

    best_logLL = float('inf')
    logLL_allIters = []
    marble_xDecomp_allIters = []
    for i in range(num_iterations): # run it for set number of iterations
        print "iter: " + str(i)
        xDecomp, xLL = marbleInstance(X, r, alpha, gamma)
        logLL_allIters.append(xLL)
        marble_xDecomp_allIters.append(xDecomp)
        if xLL > best_logLL:
            best_logLL = xLL
            best_tenDecomp = xDecomp

    return best_tenDecomp, best_logLL, marble_xDecomp_allIters, logLL_allIters    ## return the best decomposition and the LL stats; mod 20160602 RC: now, add list of all LL's -- did this for Sutter expt





'''
def run_MarbleRange(X, alpha, gamma, RANK_LOW, RANK_HIGH):
    tenDecomp, logLL = marbleInstance(X, r, alpha, gamma)
    for r in range(RANK_LOW, RANK_HIGH):
        xDecomp, xLL = marbleInstance(X, r, alpha, gamma)
        if xLL > logLL:
            logLL = xLL
            tenDecomp = xDecomp
    return tenDecomp, logLL    ## return the best decomposition and the LL stats
'''

def decomposeFile(filename, alpha, gamma, outputName, decompName, RANK, num_iterations=10):
    X, axisDict, classDict = tensorIO.load_tensor(filename)
    fileStats = defaultdict(list)
    for r in range(int(RANK), int(RANK)+1):
        tenDecomp, logLL, logLL_allIters = run_Marble(X, r, alpha, gamma, num_iterations)
        fileStats['r'].append(r)
        fileStats['ll'].append(logLL)
        fileStats['ll'].append(logLL_allIters)
        tenDecomp.save(decompName.format(r))
        fileStats['axis'] = axisDict
        with open(outputName, "wb") as outfile:
            json.dump(fileStats, outfile, indent=2)

def decomposeFileRange(filename, alpha, gamma, outputName, decompName, RANK_LOW, RANK_HIGH, num_iterations=10, store_all_iter_decomps = False):

    X, axisDict, classDict = tensorIO.load_tensor(filename)

    fileStats = defaultdict(list)
    for r in range(RANK_LOW, RANK_HIGH +1 ):
        if store_all_iter_decomps == True:
            best_tenDecomp, best_logLL, marble_xDecomp_allIters, logLL_allIters = run_Marble_returnAllIters(X, r, alpha, gamma, num_iterations)
            # tenDecomp, logLL = run_Marble(X,r, alpha, gamma, num_iterations)
            fileStats['r'].append(r)
            fileStats['ll'].append(best_logLL)
            fileStats['ll'].append(logLL_allIters)
            ## loop thru all decomps and save
            for idx in  range(len(marble_xDecomp_allIters)):
                tenDecomp = marble_xDecomp_allIters[idx]
                tenDecomp.save(decompName.format(r) + '-iter-' + str(idx))
            ## save the best decomp
            best_tenDecomp.save(decompName.format(r))
            fileStats['axis'] = axisDict
            with open(outputName, "wb") as outfile:
                json.dump(fileStats, outfile, indent=2)
                
        else:
            tenDecomp, logLL, logLL_allIters = run_Marble(X, r, alpha, gamma, num_iterations)
            # tenDecomp, logLL = run_Marble(X,r, alpha, gamma, num_iterations)
            fileStats['r'].append(r)
            fileStats['ll'].append(logLL)
            fileStats['ll'].append(logLL_allIters)
            # tenDecomp.saveDecomposition(decompName.format(r))
            ## save the best decomp
            tenDecomp.save(decompName.format(r))
            fileStats['axis'] = axisDict
            with open(outputName, "wb") as outfile:
                json.dump(fileStats, outfile, indent=2)


def run_expt(input_tensor_filename, save_results_location, output_filename_prefix, RANK,  alpha=None, gamma=None, num_iterations = 10):
    print "rank="
    print RANK
    print "alpha="
    print alpha
    print "gamma="
    print gamma
    if alpha == None:
        alpha = 25000 #default
    if gamma == None:
        gamma = [0.001, 0.01, 0.05] #default
    gamma_str = '_alpha_' + str(alpha) + '_gamma'
    for g in gamma:
        gamma_str = gamma_str + '_'
        gamma_str = gamma_str + str(g)
    filename_results_json = save_results_location + output_filename_prefix + gamma_str + ".json"
    filename_results_data = save_results_location + output_filename_prefix + gamma_str + "-{0}.dat"
    if os.path.exists(filename_results_json) or os.path.exists(filename_results_data):
        sys.exit("output filename/folder already exists!" + filename_results_json + "; " + filename_results_data)
    decomposeFile(input_tensor_filename, alpha, gamma, filename_results_json , filename_results_data, RANK, num_iterations)

def run_expt_multiple(input_tensor_filename, save_results_location, output_filename_prefix, RANK_LOW, RANK_HIGH,  alpha=None, gamma=None, num_iterations = 10):
    print "rank low="
    print RANK_LOW
    print "rank high="
    print RANK_HIGH
    print "alpha="
    print alpha
    print "gamma="
    print gamma
    if alpha == None:
        alpha = 25000 #default
    if gamma == None:
        gamma = [0.001, 0.01, 0.05] #default
    gamma_str = '_alpha_' + str(alpha) + '_gamma'
    for g in gamma:
        gamma_str = gamma_str + '_'
        gamma_str = gamma_str + str(g)

    filename_results_json = save_results_location + output_filename_prefix + gamma_str + ".json"
    filename_results_data = save_results_location + output_filename_prefix + gamma_str + "-{0}.dat"
    if os.path.exists(filename_results_json) or os.path.exists(filename_results_data):
        sys.exit("output filename/folder already exists!" + filename_results_json + "; " + filename_results_data)
    decomposeFileRange(input_tensor_filename, alpha, gamma, filename_results_json , filename_results_data, RANK_LOW, RANK_HIGH, num_iterations, store_all_iter_decomps = True) # 20160604: Sutter Heatlh;  set store_all_iter_decomps = True for debugging the "which clustering to choose" problem from Andy

