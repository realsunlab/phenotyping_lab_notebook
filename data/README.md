# data directory

This directory should contain data that would be used in specific parts of cohort construction process.

Examples:
* inclusion / exclusion criteria can be created here; they would be uploaded by the discovery or assignment pipelines
