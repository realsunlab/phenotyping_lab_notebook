# Phenotyping Lab Notebook #

This phenotyping lab notebook is a share-able digital object that empowers users with the capability to perform phenotyping tasks in a lab notebook environment. 
The current implementation is in a [Jupyter](http://jupyter.org/) notebook, however other environments such as [Zeppelin](https://zeppelin.apache.org/) notebooks will be supported in the future.

### Introduction ###

* Phenotyping Lab Notebook for Unsupervised Phenotyping from [OMOP](http://omop.org/) EHR data
* Version 1.0


### Getting Started ###

There are two major workflows:

#### Phenotype Discovery
  - In the **phenotype discovery** task, an OMOP database is connected to the pipeline and data are extracted, from which phenotypes are computed.
  - Use the notebook, phenotype_discovery_pipeline_NTF.ipynb, to perform phenotype discovery using non-negative tensor factorization. 
  - INPUT: 

           OMOP database (linked through database connector)

  - OUTPUT: 

    	    1. Phenotype definition, 
	        2. cohort, 
	    	3. exported pipelines

	    	       - Discovery pipeline
	    	       - Assignment pipeline

![phenotype_pipeline](img/phenotype_pipeline_overview.png)	       


##### Phenotyping algorithms supported:
  - Non-negative tensor factorization (Marble)
  - PheKB - Heart Failure (future)
  - PheKB - Type 2 diabetes (future)



#### Phenotype Assignment
  - In the **phenotype assignment** task, a phenotype definition previously computed with the *phenotype discovery pipeline* can be loaded, as well as an OMOP database, in order to assign patients in the query OMOP database to the phenotype definition. 
  - INPUT: 

	   1. Phenotype definition, 
	   2. OMOP database (of query patients)

  - OUTPUT: 

    	   1. Cohort (cohort of patients from the query database, belonging to the phenotype definition).


### Use Cases of the Pipeline ###

![phenotype_pipeline_use_cases](img/phenotype_pipeline_use_cases.png)	       


### General Inquiries ###

* Please contact Robert Chen (rchen87@gatech.edu). 


